#define RR 12
#define RY 11
#define RG 10

#define CWR 9
#define CWG 8

#define DELAY_Y 500
#define DELAY_G 3000
#define DELAY_BUFF 1500

void setup()
{
  Serial.begin(115200);
  for (int i = 8; i <= 12; i++)
  {
    pinMode(i, OUTPUT);
  }
  digitalWrite(RR, HIGH);
  digitalWrite(CWR, HIGH);
}

void loop()
{
  // Red
  digitalWrite(RR, HIGH);
  delay(DELAY_BUFF);
  // Red yellow
  digitalWrite(RY, HIGH);
  delay(DELAY_Y);
  // Green
  digitalWrite(RY, LOW);
  digitalWrite(RR, LOW);
  digitalWrite(RG, HIGH);
  delay(DELAY_G);
  // Yellow
  digitalWrite(RG, LOW);
  digitalWrite(RY, HIGH);
  delay(DELAY_Y);
  // Red
  digitalWrite(RY, LOW);
  digitalWrite(RR, HIGH);
  delay(DELAY_BUFF);
  // CW Go
  digitalWrite(CWR, LOW);
  digitalWrite(CWG, HIGH);
  delay(DELAY_G);
  // CW Blink
  for (int i = 0; i < 10; i++)
  {
    digitalWrite(CWG, LOW);
    delay(100);
    digitalWrite(CWG, HIGH);
    delay(100);
  }
  // CW Stop
  digitalWrite(CWR, HIGH);
  digitalWrite(CWG, LOW);

  Serial.println("Cycle complete");
}
