# Wokwi - Crosswalk with traffic lights

The project simulates a crosswalk with traffic lights.

## Cycle

* Red
* Red & Yellow
* Green
* Yellow
* Red
* CW green
* CW green blinking
* CW red
* Yellow 

## Screenshot

![screenshot](screenshot.png)


## Contributors

Eivind Vold Aunebakk
